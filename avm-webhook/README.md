# AVM Webhook Service for AWCY

This is a simple service which runs in the AWCY server, which does few things
+ Fetches the current Commit ID of HEAD 
+ Fetches beta runs and look for nightly jobs
+ Check if the commit ID of HEAD present or not
+ Submit a nightly encode job if it is not there
+ Repeat this for every 24 hours
