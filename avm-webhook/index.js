var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();
var config = require('./config.json');
const fetch = require('node-fetch');

app.use(bodyParser.json());

// URL of the GitLab API to get the current head commit
const gitLabApiUrl = 'https://gitlab.com/api/v4/projects/28773770/repository/commits/main';
// URL of the AWCY Beta
const apiUrl = "https://beta.arewecompressedyet.com/list.json";

// Function to get the current head commit from GitLab
async function getCurrentCommit() {
    try {
        const response = await fetch(gitLabApiUrl);
        const commitData = await response.json();
        const currentCommit = commitData.id;

        return currentCommit;
    } catch (error) {
        console.error('Error fetching current commit:', error);
        return null;
    }
}

// Function to fetch runs from AWCY
async function fetchRuns() {
    try {
        const response = await fetch(apiUrl);
        const runList = await response.json();
        return runList;
    } catch (error) {
        console.error('Error fetching runs:', error);
        return [];
    }
}

// Return Nigthly runs for AVM
function filterNightlyRuns(runs) {
    // Filter runs with "nightly" in run_id and "av2-ra-st" which is
    // (completed/building/waiting/new) and not failed:)
    return runs.filter(run =>
        run.run_id.includes("nightly")
        && run.info.codec === "av2-ra-st"
        && (run.status === "waiting"
            || run.status === "completed"
            || run.status === "building"
            || run.status === "running"
            || run.status === "new")
        && run.failed === false
    );
}


// Main function
async function main() {

    // Get the current head commit
    const currentCommit = await getCurrentCommit();

    if (currentCommit) {
        console.log('Current head commit:', currentCommit);
        // Fetch runs from AWCY Run List
        const runList = await fetchRuns();
        // Filter AVM-Nightly runs
        const nightlyRuns = filterNightlyRuns(runList);
        // Check if the current HEAD commit of AVM commit matches any of the runs
        const commitJobs = nightlyRuns.filter(run => run.info.commit === currentCommit)
        // Run ID 
        var today = new Date();
        var date_to_add = String(today.toISOString());
        var run_id = 'AVM-main-ra-mandatory-nightly-' + currentCommit.substring(0, 8) + '-' + date_to_add;
        if (commitJobs.length <= 0) {
            // Submit the job with the current commit as there is no jobs
            request.post('https://beta.arewecompressedyet.com/submit/job', {
                form:
                {
                    run_id: run_id,
                    commit: currentCommit,
                    master: true,
                    key: config.key,
                    codec: 'av2-ra-st',
                    task: 'aomctc-a2-2k',
                    extra_options: '--verbose --limit=17',
                    ctcSets: 'aomctc-mandatory',
                    ctcPresets: 'av2-ra-st',
                    ctcVersion: '5.0'
                }
            });
            console.log('Current commit do not match a nightly run. Submitted a job.');
        } else {
            console.log('Current commit matches a nightly run. Skipping job submission.');
        }
    } else {
        console.log('Failed to retrieve the current commit.');
    }
}

main();
// 86400000 == 24 hours
setInterval(main, 86400000);

