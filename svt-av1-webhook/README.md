# SVT-AV1 Webhook Service for AWCY

This is a simple service which runs in the AWCY server, which does few things
+ Fetches the current Commit ID of HEAD 
+ Fetches beta runs and look for nightly jobs
+ Check if the commit ID of HEAD present or not
+ Submit a nightly encode job if it is not there
+ Repeat this for every 24 hours

## Nightly Config
+ Set: [elfluente-1080p-as](https://media.xiph.org/video/aomctc/siwg_ctc/)
+ CLI: Default Encoder as [1-Pass](https://github.com/xiph/rd_tool/blob/master/metrics_gather.sh#L557)
+ Presets: 2, 4, 6, 8, 10
