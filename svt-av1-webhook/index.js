var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();
var config = require('./config.json');
const fetch = require('node-fetch');

app.use(bodyParser.json());

// URL of the GitLab API to get the current head commit
const gitLabApiUrl = 'https://gitlab.com/api/v4/projects/24327400/repository/commits/master';
// URL of the AWCY Beta
const apiUrl = "https://beta.arewecompressedyet.com/list.json";

// Function to get the current head commit from GitLab
async function getCurrentCommit() {
    try {
        const response = await fetch(gitLabApiUrl);
        const commitData = await response.json();
        const currentCommit = commitData.id;

        return currentCommit;
    } catch (error) {
        console.error('Error fetching current commit:', error);
        return null;
    }
}

// Function to fetch runs from AWCY
async function fetchRuns() {
    try {
        const response = await fetch(apiUrl);
        const runList = await response.json();
        return runList;
    } catch (error) {
        console.error('Error fetching runs:', error);
        return [];
    }
}

// Return Nigthly runs for SVT-AV1
function filterNightlyRuns(runs) {
    // Filter runs with "nightly" in run_id and "svt-av1-as" which is
    // (completed/building/waiting/new) and not failed:)
    return runs.filter(run =>
        run.run_id.includes("nightly")
        &&
        run.info.task == "elfuente-1080p-as"
        &&
        run.info.codec === "svt-av1-as"
        && (run.status === "waiting"
            || run.status === "completed"
            || run.status === "building"
            || run.status === "running"
            || run.status === "new")
        && run.failed === false
    );
}


// Main function
async function main() {

    // Get the current head commit
    const currentCommit = await getCurrentCommit();

    if (currentCommit) {
        console.log('Current head commit:', currentCommit);
        // Fetch runs from AWCY Run List
        const runList = await fetchRuns();
        // Filter SVT-AV1-Nightly runs
        const nightlyRuns = filterNightlyRuns(runList);
        // Check if the current HEAD commit of SVT-AV1 commit matches any of the runs
        const commitJobs = nightlyRuns.filter(run => run.info.commit === currentCommit)
        // Run ID
        var today = new Date();
        var date_to_add = String(today.toISOString());
        var run_id_prefix = 'SVT-AV1-master-elfuente-1080p-as-nightly-' + currentCommit.substring(0, 8) + '-';

        // List of Presets to test
        const presets = [2, 4, 6, 8, 10];

        // Create an array of promises representing the requests
        const requests = presets.map(async (preset) => {
            var run_id = run_id_prefix + 'preset' + preset + '-' + date_to_add;
            const presetJobExists = commitJobs.some(run => run.info.extra_options.includes(`--preset ${preset}`));

            if (!presetJobExists) {
                // Return the request promise without submitting
                return request.post('https://beta.arewecompressedyet.com/submit/job', {
                    form: {
                        run_id: run_id,
                        commit: currentCommit,
                        master: true,
                        key: config.key,
                        codec: 'svt-av1-as',
                        task: 'elfuente-1080p-as',
                        extra_options: '--progress 2 --preset ' + preset,
                        ctcVersion: '5.0'
                    }
                });
            } else {
                console.log(`A nightly run already exists for preset ${preset}. Skipping job submission.`);
                // Return a resolved promise
                return Promise.resolve();
            }
        });
        // Use a loop to submit the requests
        for (const requestPromise of requests) {
            await requestPromise; // Await each request
        }

    } else {
        console.log('Failed to retrieve the current commit.');
    }
}

main();
// 86400000 == 24 hours
setInterval(main, 86400000);
