# AWCY Nightly Job Configuration and Webhooks

## AVM
+ Setup: [AVM Web Hook](avm-webhook)
+ Configuraiton: [AWCY Wiki](https://github.com/xiph/awcy/wiki/AOM:-User-Manual-for-doing-AOM-CTC-development-in-AWCY#avm-nightly-jobs)


## SVT-AV1
+ Setup: [SVT-AV1 Web Hook](svt-av1-webhook)
+ Configuraiton: [AWCY Wiki](https://github.com/xiph/awcy/wiki/AOM:-User-Manual-for-doing-AOM-CTC-development-in-AWCY#svt-av1-periodic-encode-jobs)
